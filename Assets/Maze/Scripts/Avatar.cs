﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ExitGames.Client.Photon;
using HashTable = ExitGames.Client.Photon.Hashtable;

public class Avatar : Photon.MonoBehaviour
{

    [SerializeField] int m_healthPoint = 0;
    [SerializeField] GameObject m_BulletPrefab;
    [SerializeField] GameObject m_DamageEffectPrefab;

    PhotonView m_photonView;

    void Start()
    {
        m_photonView = GetComponent<PhotonView>();
        if (m_photonView.isMine)
        {
            // 初期設定
            SetPlayerName("Player-Id: " + PhotonNetwork.player.ID);
            SetPlayerDeathState(false);
            SetHealthPoint(m_healthPoint);

        }
        SetColor(m_photonView.ownerId);
    }

    private Color[] PLAYER_COLOR = new Color[] { Color.white, Color.red, Color.green, Color.yellow };

    void SetColor(int playerid)
    {
        if (playerid > 3) { playerid = playerid % 3 + 1; }
        ChangeColor(PLAYER_COLOR[playerid - 1]);

        /*
        ExitGames.Client.Photon.Hashtable ht = PhotonNetwork.player.CustomProperties;
        if (ht == null)
        {
            ht = new HashTable();
        }
        ht["id"] = playerid;
        */
    }

    void ChangeColor(Color col)
    {
        var palent = gameObject.transform.Find("TankRenderers");
        foreach (Transform trs in palent.gameObject.transform)
        {
            if (trs.GetComponent<Renderer>())
            {
                trs.GetComponent<Renderer>().material.color = col;
            }
        }
    }

    void SetPlayerName(string name)
    {
        //this.gameObject.name = name;
        //transform.Find("NameUI").gameObject.GetComponent<TextMesh>().text = name;
    }

    void SetHealthPoint(int num)
    {
        //m_healthPoint = num;
        //transform.Find("HealthPointUI").gameObject.GetComponent<TextMesh>().text = num.ToString();
    }

    // ストリーム同期
    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        print("stream:" + stream.isReading);

        if (stream.isWriting)
        {
            // 自分の情報を送る
            string myName = this.gameObject.name;
            int myHealthPoint = m_healthPoint;
            stream.SendNext(myName);
            stream.SendNext(myHealthPoint);
        }
        else
        {
            // 他人の情報を受け取る
            string otherName = (string)stream.ReceiveNext();
            int otherHealthPoint = (int)stream.ReceiveNext();
            SetPlayerName(otherName);
            SetHealthPoint(otherHealthPoint);
        }
    }

    // イベント同期
    [PunRPC]
    void Shoot(Vector3 i_pos, Vector3 i_angle)
    {
        // 全クライアントの中の自分が一斉に弾丸を発射する
        Quaternion rot = Quaternion.Euler(i_angle);
        GameObject bullet = GameObject.Instantiate(m_BulletPrefab, i_pos, rot);
        bullet.GetComponent<Rigidbody>().AddForce(transform.forward * 20, ForceMode.VelocityChange);
        Destroy(bullet, 3);
    }

    void OnTriggerEnter(Collider i_other)
    {
        if (i_other.tag == "Oil")
        {
            // ダメージを受ける
            SetHealthPoint(m_healthPoint + 10);
            //Instantiate(m_DamageEffectPrefab, transform.position, Quaternion.identity);
            //Destroy(i_other.gameObject);
            //PhotonNetwork.Destroy(i_other.gameObject);

            i_other.gameObject.GetComponent<INetworkDestroy>().DestroyMe();

            if (m_healthPoint <= 0)
            {
                if (m_photonView.isMine)
                {
                    // gameover
                    SetPlayerDeathState(true);
                    string text = "You Lose...";
                    DrawResult(text, new Color(0f, 0f, 1f));
                }
            }
        }
    }

    void Update()
    {
        if (!GameManager.Instance.AvatarCanMove) return;

        if (m_photonView.isMine == false)
        {
            return;
        }
        /*
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // 自分自身の弾丸でトリガーを引かないようにやや手前から発射
            Vector3 pos = transform.position + transform.forward * 2f;
            Vector3 angle = transform.eulerAngles;
            // 処理が重いのでPhotonViewを付けずに位置と角度を渡す
            m_photonView.RPC("Shoot", PhotonTargets.AllViaServer, pos, angle);
        }
        */

        // 自分のアバターだけを動かす
        var v = Input.GetAxis("Vertical");
        Vector3 velocity = new Vector3(0, 0, v);
        velocity = transform.TransformDirection(velocity);
        velocity *= 5f;
        //transform.localPosition += velocity * Time.fixedDeltaTime;
        //gameObject.GetComponent<Rigidbody>().AddForce(velocity,ForceMode.Acceleration);
        gameObject.GetComponent<Rigidbody>().velocity = velocity;

        var h = Input.GetAxis("Horizontal");
        transform.Rotate(0, h * 3f, 0);

    }

    // ===============================
    // ここから重要
    // ===============================
    public void SetPlayerDeathState(bool isDeath)
    {
        var properties = new ExitGames.Client.Photon.Hashtable();
        properties.Add("player-id", PhotonNetwork.player.ID);
        properties.Add("isDeath", isDeath);
        PhotonNetwork.player.SetCustomProperties(properties);
    }

    bool CanMove = false;

    public void OnPhotonPlayerPropertiesChanged(object[] i_playerAndUpdatedProps)
    {
        /*
        foreach (var p in PhotonNetwork.playerList)
        {
            CanMove = (bool)p.CustomProperties["CanMove"];
        }
        */

        /*
        // 全員分回す
        var aliveList = new ArrayList();
        var deathList = new ArrayList();
        foreach (var p in PhotonNetwork.playerList)
        {
            Debug.Log("player-id : " + p.CustomProperties["player-id"].ToString() + " - " + "isDeath : " + p.CustomProperties["isDeath"].ToString());
            if ((bool)p.CustomProperties["isDeath"])
            {
                deathList.Add(p);
            }
            else
            {
                aliveList.Add(p);
            }
        }
        if (aliveList.Count == 1 && deathList.Count > 0)
        {
            var winner = (PhotonPlayer)aliveList[0];
            var winnerId = winner.CustomProperties["player-id"];
            if ((int)PhotonNetwork.player.ID == (int)winnerId)
            {
                string text = "プレイヤー" + winnerId.ToString() + " の勝利";
                DrawResult(text, new Color(1f, 0f, 0f));
            }
        }
        */
    }

    void DrawResult(string text, Color color)
    {
        Text ResultText = GameObject.Find("ResultText").GetComponent<Text>();
        ResultText.text = text;
        ResultText.color = color;
    }

}