﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;
using HashTable = ExitGames.Client.Photon.Hashtable;
using UnityEngine.UI;

public class GameManager : SingletonMonoBehaviour<GameManager> {

    [SerializeField]
    GameObject wallPrefab;
    [SerializeField]
    GameObject wallOutPrefab;
    [SerializeField]
    Text TimerText;
    [SerializeField]
    Text MessageText;
    [SerializeField]
    Button ButtonContinue;
    [SerializeField]
    Button ButtonQuit;
    [SerializeField]
    GameObject InstractionPanel;
    [SerializeField]
    Button ButtonReboot;

    public enum EnumGameState
    {
        None,
        Waiting,
        Playing,
        Resulting
    }

    static int InitialPlayTime = 45;

    int PlayTime = 0;
    int WaitTime = 0;
    int WaitContinueTime = 0;

    public EnumGameState state = EnumGameState.None;
    EnumGameState newState = EnumGameState.None;

    // Use this for initialization
    void Start()
    {
        ButtonContinue.gameObject.SetActive(false);
        ButtonQuit.gameObject.SetActive(false);
        ButtonReboot.gameObject.SetActive(false);

        //BuildStage();
        SetMessageText("ゲームを準備中です");
    }

    // Update is called once per frame
    void Update()
    {
        switch (newState)
        {
            case EnumGameState.None:
                break;

            case EnumGameState.Waiting:
                InstractionPanel.SetActive(true);

                PlayTime = InitialPlayTime;
                SetRoomProp();

                BuildStage();

                AvatarCanMove = false;
                SetMessageText("参加者を募集中です");
                PhotonNetwork.room.IsOpen = true;
                if (PhotonNetwork.isMasterClient)
                {
                    WaitTime = 20;
                    SetRoomProp();

                    StartCoroutine("WaitTimer");
                }
                break;

            case EnumGameState.Playing:
                InstractionPanel.SetActive(false);

                AvatarCanMove = true;
                PhotonNetwork.room.IsOpen = false;

                if (PhotonNetwork.isMasterClient)
                {
                    StartCoroutine("PlayTimer");
                }
                SetMessageText("時間内に全てのオイルを回収しよう！(操作:WASD)");
                break;

            case EnumGameState.Resulting:

                if (PhotonNetwork.isMasterClient)
                {
                    StopCoroutine("PlayTimer");
                }

                AvatarCanMove = false;
                ShowWinner();

                ButtonContinue.gameObject.SetActive(true);
                ButtonQuit.gameObject.SetActive(true);
                WaitContinueTime = 15;

                StartCoroutine("ForceSelect");

                break;

            default:
                break;
        }

        if (newState != EnumGameState.None)
        {
            state = newState;
            newState = EnumGameState.None;
        }

        if (state == EnumGameState.Playing)
        {
            if (GameObject.FindGameObjectWithTag("Oil") == null)
            {
                Debug.Log("All Oil has been taken");
                newState = EnumGameState.Resulting;
            }

            if (PlayTime <= 0)
            {
                newState = EnumGameState.Resulting;
            }
        }
        else if (state == EnumGameState.Waiting)
        {
            if (WaitTime <= 0)
            {
                newState = EnumGameState.Playing;
            }
        }
    }

    IEnumerator ForceSelect()
    {
        while (WaitContinueTime > 0)
        {
            WaitContinueTime--;
            //SetMessageText("");
            TimerText.text = string.Format("続けますか？終了しますか？（あと{0}秒で終了）", WaitContinueTime);
            yield return new WaitForSeconds(1);
        }
        Quit();
    }

    void ShowWinner()
    {
        if (PlayTime <= 0)
        {
            SetMessageText("時間切れです。友達と協力してクリアしよう！");
        }
        else
        {
            SetMessageText(string.Format("ギリギリ残{0}秒でクリアした！今夜はカツゲンだ！", PlayTime));
        }
    }

    public void onClick_Continue()
    {
        StopCoroutine("ForceSelect");
        newState = EnumGameState.Waiting;
        ButtonContinue.gameObject.SetActive(false);
        ButtonQuit.gameObject.SetActive(false);
    }

    public void onClick_Quit()
    {
        Quit();
    }

    void Quit()
    {
        StopCoroutine("ForceSelect");
        ButtonContinue.gameObject.SetActive(false);
        ButtonQuit.gameObject.SetActive(false);
        ButtonReboot.gameObject.SetActive(true);

        PhotonNetwork.Disconnect();

        SetMessageText("切断しました。再度プレイするには「再接続」をクリックしてください。");
        TimerText.text = "";
    }

    public void onClick_Reboot()
    {
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("Main");
    }

    public bool AvatarCanMove = false;
    
    IEnumerator PlayTimer()
    {
        while (PlayTime > 0)
        {
            PlayTime--;
            SetRoomProp();

            yield return new WaitForSeconds(1);
        }
    }

    IEnumerator WaitTimer()
    {
        while (WaitTime > 0)
        {
            WaitTime--;
            SetRoomProp();

            yield return new WaitForSeconds(1);
        }
    }

    void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {
        Debug.Log("MasterClient switched");
        SetMessageText("ゲームマスターが退出したため、5秒後にゲームを再起動します");
        StartCoroutine(Reboot());
    }

    IEnumerator Reboot()
    {
        yield return new WaitForSeconds(5);
        PhotonNetwork.Disconnect();
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("Main");
    }

    void GetRoomProp(ExitGames.Client.Photon.Hashtable changed)
    {
        {
            object value = null;
            if (changed.TryGetValue("RestTime", out value))
            {
                PlayTime = (int)value;
            }
        }
        {
            object value = null;
            if (changed.TryGetValue("WaitTime", out value))
            {
                WaitTime = (int)value;
            }
        }
    }

    void SetRoomProp()
    {
        HashTable ht = new HashTable();
        ht["RestTime"] = PlayTime;
        ht["WaitTime"] = WaitTime;
        PhotonNetwork.room.SetCustomProperties(ht);
    }

    public void OnPhotonCustomRoomPropertiesChanged(ExitGames.Client.Photon.Hashtable i_propertiesThatChanged)
    {
        print("OnPhotonRoomPropertiesChanged Called");
        GetRoomProp(i_propertiesThatChanged);

        if (state == EnumGameState.Waiting)
        {
            TimerText.text = "開始まで" + WaitTime.ToString() + "秒";
        }
        else if (state == EnumGameState.Playing)
        {
            TimerText.text = "残り" + PlayTime.ToString() + "秒";
        }
    }

    public void SetMessageText(string msg)
    {
        MessageText.text = msg;
    }

    void DestroyWithTag(string tag)
    {
        var gos = GameObject.FindGameObjectsWithTag(tag);
        for (int i = 0; i < gos.Length; i++)
        {
            gos[i].GetComponent<INetworkDestroy>().DestroyMe();
        }
    }

    public void BuildStage()
    {
        if (PhotonNetwork.isMasterClient)
        {
            DestroyWithTag("Oil");
            DestroyWithTag("WallSync");
        }

        int sizeX = 18;
        int sizeZ = 18; //must be even

        int rand = 0;
        bool IsHall = false;

        for (int z = 0; z <= sizeZ; z++)
        {
            IsHall = false;

            for (int x = 0; x <= sizeX; x++)
            {
                if (x == 0 || x == sizeX || z == 0 || z == sizeZ)
                {
                    Instantiate(wallOutPrefab, new Vector3(x, 0, z), Quaternion.identity);
                }
                else if (x % 2 == 0 && z % 2 == 0)
                {
                    Instantiate(wallPrefab, new Vector3(x, 0, z), Quaternion.identity);
                }
                else
                {
                    if (PhotonNetwork.isMasterClient)
                    {
                        if (z > 1 && z < sizeZ && x > 0 && x < sizeX)
                        {
                            if (z % 2 == 0 && x % 2 == 1)
                            {
                                if (x == sizeX - 1 && IsHall == false)
                                {
                                    Debug.Log("Create Hall");
                                    //continue;
                                }
                                else
                                {
                                    rand = Random.Range(0, 2);
                                    if (rand == 0)
                                    {
                                        PhotonNetwork.Instantiate("WallSync", new Vector3(x, 0, z), Quaternion.identity, 0);
                                    }
                                    else
                                    {
                                        IsHall = true;
                                    }
                                }
                            }
                            else if (z % 2 == 1)
                            {
                                rand = Random.Range(0, 2);
                                if (rand == 1)
                                {
                                    PhotonNetwork.Instantiate("OilStorage", new Vector3(x, -0.5f, z), Quaternion.identity, 0);
                                }
                            }
                        }

                    }
                }
            }
        }

        StartCoroutine(WaitForBuild());
    }

    IEnumerator WaitForBuild()
    {
        yield return new WaitForSeconds(3);
    }
}
