﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HashTable = ExitGames.Client.Photon.Hashtable;

public class PhotonLogin : MonoBehaviour
{
    [SerializeField]
    string AvatarPrefabName = "";
    [SerializeField]
    GameObject PlayerParent;

    private const string ROOM_NAME = "VR-Room";
    private const string GAME_VERSION = "v1.0";

    void Start()
    {
        Debug.Log("PhotonManager: ロビーに接続");
        PhotonNetwork.ConnectUsingSettings(GAME_VERSION);
    }

    void OnJoinedLobby()
    {
        Debug.Log("PhotonManager: ロビーに入室成功");

        JoinRoom();
    }

    void JoinRoom()
    {
        RoomOptions roomOptions = new RoomOptions()
        {
            MaxPlayers = 20,
            IsOpen = true,
            IsVisible = true,
            CustomRoomPropertiesForLobby = new string[] { "RestTime", "WaitTime" }
        };

        PhotonNetwork.JoinOrCreateRoom(ROOM_NAME, roomOptions, null);
    }

    public void JoinRoom(string playerName)
    {
        PlayerName = playerName;

        JoinRoom();
    }

    string PlayerName = "";

    void OnJoinedRoom()
    {
        if (AttemptToJoinRoom)
        {
            StopCoroutine("JoinRoomCoroutine");
        }
        AttemptToJoinRoom = false;

        PhotonNetwork.player.NickName = PlayerName;

        Room room = PhotonNetwork.room;
        PhotonPlayer player = PhotonNetwork.player;


        Debug.Log("PhotonManager: ルーム入室成功　部屋名:" + room.Name + ", プレイヤーID:" + player.ID);
        Debug.Log("PhotonManager: 部屋情報:" + room + ", ルームマスター？:" + player.IsMasterClient);

        CreateAvatar();

        GameManager.Instance.onClick_Continue();
    }

    void CreateAvatar()
    {
        Debug.Log("Player: 自分のアバターを生成");

        int PlayerNum = PhotonNetwork.playerList.Length;

        var guide = PlayerParent.transform.position;
        Vector3 pos = new Vector3(guide.x + (PlayerNum - 1) * 1f, guide.y, guide.z);

        GameObject avatar = PhotonNetwork.Instantiate(AvatarPrefabName, pos, Quaternion.identity, 0);
        Camera.main.transform.SetParent(avatar.transform, true);
        Camera.main.transform.localPosition = new Vector3(0f, 20f, 0f);
    }

    void ChangeColor()
    {
        foreach (var p in PhotonNetwork.playerList)
        {
        }
    }

    void OnPhotonJoinRoomFailed()
    {
        Debug.Log("PhotonManager: ルーム入室失敗");

        foreach (var r in PhotonNetwork.GetRoomList())
        {
            if (r.Name == "VR-Room")
            {
                int time = (int)r.CustomProperties["RestTime"];
                GameManager.Instance.SetMessageText(string.Format("次のゲーム開始まで{0}秒ほどお待ちください", time));
            }
        }

        if (AttemptToJoinRoom == false)
        {

            StartCoroutine("JoinRoomCoroutine");
            AttemptToJoinRoom = true;
        }
    }

    bool AttemptToJoinRoom = false;

    IEnumerator JoinRoomCoroutine()
    {
        while (true)
        {
            JoinRoom();
            yield return new WaitForSeconds(2);
        }
    }

    void OnPhotonCreateRoomFailed()
    {
        Debug.Log("PhotonManager: ルーム作成失敗");
    }
}