﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonStatus : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            isShow = !isShow;
        }
    }

    bool isShow = false;

    void OnGUI()
    {
        if (!isShow) return;

        string status = "Photon: " + PhotonNetwork.connectionStateDetailed.ToString() + "\n";
        if (PhotonNetwork.inRoom)
        {
            status += "---------------------------------------------------";
            status += "Room Name: " + PhotonNetwork.room.Name + "\n";
            status += "Player Num: " + PhotonNetwork.room.PlayerCount + "\n";
            status += "---------------------------------------------------";
            status += "Player Id: " + PhotonNetwork.player.ID + "\n";
            status += "IsMasterClient: " + PhotonNetwork.isMasterClient + "\n";
        }
        GUI.TextField(new Rect(10, 10, 220, 120), status);
    }

}