﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallSync : MonoBehaviour,INetworkDestroy {

    public void DestroyMe()
    {
        GetComponent<PhotonView>().RPC("DestroyWrapper", PhotonTargets.All);
    }

    [PunRPC]
    void DestroyWrapper()
    {
        if (GetComponent<PhotonView>().isMine)
        {
            PhotonNetwork.Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
